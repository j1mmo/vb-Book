﻿Imports System
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Collections.Generic

Module Project

    public Structure item
        public isbn  As String
        public title As String
        public price As Decimal
    End Structure

    public WithEvents Dim first_btn   As New Button()
    public WithEvents Dim prev_btn    As New Button()
    public WithEvents Dim last_btn    As New Button()
    public WithEvents Dim next_btn    As New Button()
    public WithEvents Dim new_btn     As New Button()
    public WithEvents Dim edit_btn    As New Button()
    public WithEvents Dim save_btn    As New Button()
    public WithEvents Dim delete_btn  As New Button()
    
    public WithEvents Dim title_tb    As New TextBox()
    public WithEvents Dim isbn_tb     As New TextBox()
    public WithEvents Dim price_tb    As New TextBox()
    
    public WithEvents Dim isbn_label  As New Label()
    public WithEvents Dim title_label As New Label()
    public WithEvents Dim price_label As New Label()
    
    public WithEvents dim books as new List(of item)
    public dim index as new Integer
    public dim edit_enabled as boolean
    
    public WithEvents Dim f As New Form()
    Sub Main()
        'create form'

        
        f.Text = "Book Management System"
        f.Width = 885
        f.Height = 600
        f.StartPosition = FormStartPosition.CenterScreen

        'create buttons'
        
        first_btn.Text = "First"
        first_btn.Width = 100
        first_btn.Height = 100
        first_btn.BackColor = Color.Orange
        first_btn.Location = New Point(20, 20)

        
        prev_btn.Text = "Prev"
        prev_btn.Width = 100
        prev_btn.Height = 100
        prev_btn.BackColor = Color.Orange
        prev_btn.Location = New Point(125, 20)

        
        next_btn.Text = "Next"
        next_btn.Width = 100
        next_btn.Height = 100
        next_btn.BackColor = Color.Orange
        next_btn.Location = New Point(230, 20)

        
        last_btn.Text = "Last"
        last_btn.Width = 100
        last_btn.Height = 100
        last_btn.BackColor = Color.Orange
        last_btn.Location = New Point(335, 20)

        
        new_btn.Text = "New"
        new_btn.Width = 100
        new_btn.Height = 100
        new_btn.BackColor = Color.Orange
        new_btn.Location = New Point(445, 20)

        
        edit_btn.Text = "Edit"
        edit_btn.Width = 100
        edit_btn.Height = 100
        edit_btn.BackColor = Color.Orange
        edit_btn.Location = New Point(550, 20)

        
        save_btn.Text = "Save"
        save_btn.Width = 100
        save_btn.Height = 100
        save_btn.BackColor = Color.Orange
        save_btn.Location = New Point(655, 20)

        
        delete_btn.Text = "Delete"
        delete_btn.Width = 100
        delete_btn.Height = 100
        delete_btn.BackColor = Color.Orange
        delete_btn.Location = New Point(760, 20)

        'create text boxes & labels'

        
        isbn_tb.Width = 200
        isbn_tb.Height = 30
        isbn_tb.BackColor = Color.White
        isbn_tb.Location = New Point(200, 200)
        isbn_tb.Enabled = false

        
        isbn_label.Width = 40
        isbn_label.Height = 25
        isbn_label.Text = "ISBN:"
        isbn_label.Location = New Point(150, 200)

        
        title_tb.Width = 200
        title_tb.Height = 30
        title_tb.BackColor = Color.White
        title_tb.Location = New Point(200, 230)
        title_tb.Enabled = false
        

        
        title_label.Width = 40
        title_label.Height = 25
        title_label.Text = "Title:"
        title_label.Location = New Point(150, 230)

        
        price_tb.Width = 200
        price_tb.Height = 30
        price_tb.BackColor = Color.White
        price_tb.Location = New Point(200, 260)
        price_tb.Enabled = false

        
        price_label.Width = 40
        price_label.Height = 25
        price_label.Text = "price:"
        price_label.Location = New Point(150, 260)

        'add buttons program'
        f.AcceptButton = first_btn
        f.AcceptButton = prev_btn
        f.AcceptButton = next_btn
        f.AcceptButton = last_btn

        f.AcceptButton = new_btn
        f.AcceptButton = edit_btn
        f.AcceptButton = save_btn
        f.AcceptButton = delete_btn

        f.Controls.Add(first_btn)
        f.Controls.Add(prev_btn)
        f.Controls.Add(next_btn)
        f.Controls.Add(last_btn)

        f.Controls.Add(new_btn)
        f.Controls.Add(edit_btn)
        f.Controls.Add(save_btn)
        f.Controls.Add(delete_btn)

        'add the labels & text boxes for new items'
        f.Controls.Add(isbn_tb)
        f.Controls.Add(title_tb)
        f.Controls.Add(price_tb)

        f.Controls.Add(isbn_label)
        f.Controls.Add(title_label)
        f.Controls.Add(price_label)
        
        first_btn.Enabled = false
        prev_btn.Enabled = false  
        last_btn.Enabled = false 
        next_btn.Enabled = false  
        edit_btn.Enabled = false  
        save_btn.Enabled = false   
        delete_btn.Enabled = false
        
        new_btn.Enabled = true
        
        edit_enabled = false
       
        Application.Run(f)
    End Sub
    

    Private Sub enable_text(sender As Object, args As EventArgs) Handles new_btn.Click, edit_btn.Click
         
        title_tb.Enabled = true
        isbn_tb.Enabled = true
        price_tb.Enabled = true
        
    End Sub
    
    Private Sub disable_text(sender As Object, args As EventArgs) Handles first_btn.Click, prev_btn.Click, last_btn.Click, next_btn.Click
        title_tb.Enabled = false
        isbn_tb.Enabled = false
        price_tb.Enabled = false
        
        delete_btn.Enabled = true
        edit_btn.Enabled = true
        new_btn.Enabled = true
        save_btn.Enabled = false
    End Sub
    
    private Sub new_book(sender as object, args as EventArgs) handles new_btn.Click
        edit_btn.Enabled = false   
        delete_btn.Enabled = false
        new_btn.Enabled = false
        save_btn.Enabled = true  
        
        isbn_tb.Text = ""
        title_tb.Text = ""
        price_tb.Text = ""
    end sub
    
    private Sub save_book(sender As Object, args As EventArgs) Handles save_btn.Click
   
        new_btn.Enabled = true
        save_btn.Enabled = true
        
        dim correct_input as boolean = true
        dim error_msg as string = ""
        dim isbn_already_exists as item = books.Find(Function(b as item) 
                                                        return b.isbn = isbn_tb.Text 
                                                     end function)
                                                     
       if isbn_already_exists.isbn <> nothing and edit_enabled = false
            correct_input = false
            error_msg += "isbm entered alread exisits" + Environment.NewLine
       end if
       
       if isbn_tb.Text.Length <> 13
            correct_input = false
            error_msg += "incorrect length of isbn" + Environment.NewLine
       end if
       
       if isbn_tb.Text = "" or title_tb.Text = "" or price_tb.Text = ""
           correct_input = false
            error_msg += "Some input boxes are empty" + Environment.NewLine 
       end if
       
       for each c as Char in isbn_tb.Text
            if Char.IsDigit(c) = false 
                correct_input = false
                error_msg += "isbn does not contain characters: " + c + Environment.NewLine
                exit for
            end if 
       next
        
       
       if correct_input = true
            Try
                if edit_enabled = true
                    books.Remove(isbn_already_exists)
                end if
                
                dim new_book as item
                new_book.isbn = isbn_tb.Text
                new_book.title = title_tb.Text
                new_book.price = Decimal.Parse(price_tb.Text)
                books.Add(new_book)
                
                first_btn.Enabled = true
                prev_btn.Enabled = true  
                last_btn.Enabled = true 
                next_btn.Enabled = true
            
                isbn_tb.Text = ""
                title_tb.Text = ""
                price_tb.Text = ""
            Catch ex As Exception
                dim e as string
                e = "Error: "
                e += ex.Message
                MessageBox.Show(e, ex.Message)
            End Try
        else
            MessageBox.Show(error_msg, "Input Error")
        end if
    End Sub
    
    private Sub first_fn(sender as object, args as EventArgs) Handles first_btn.Click
        
        index = 0
        if books.Count > 0
            dim first_book as item = books(index)
            isbn_tb.Text = first_book.isbn
            title_tb.Text = first_book.title
            price_tb.Text = String.Format("{0:00}", first_book.price)
        else
            MessageBox.Show("List is empty")
        end if
        
    end sub
    
    private sub next_fn(sender as object, args as EventArgs) Handles next_btn.Click
        if index <> (books.Count - 1)
            dim last_book as item = books(index + 1)
            isbn_tb.Text = last_book.isbn
            title_tb.Text = last_book.title
            price_tb.Text = String.Format("{0:00}", last_book.price)
            index += 1
        end if
    end sub
    
    private sub prev_fn(sender as object, args as EventArgs) Handles prev_btn.Click
        if index > 0
            dim last_book as item = books(index - 1)
            isbn_tb.Text = last_book.isbn
            title_tb.Text = last_book.title
            price_tb.Text = String.Format("{0:00}", last_book.price)
            index -= 1
        end if 
    end sub
    
    private sub last_fn(sender as object, args as EventArgs) Handles last_btn.Click
        
        if books.Count <> 0
            index = books.Count - 1
            dim last_book as item = books(index)
            isbn_tb.Text = last_book.isbn
            title_tb.Text = last_book.title
            price_tb.Text = String.Format("{0:00}", last_book.price)
        else
            MessageBox.Show("list is empty")
        end if
    end sub
    
    private sub delete_fn(sender as object, args as EventArgs) Handles delete_btn.Click
        
        if isbn_tb.Text <> "" and title_tb.Text <> "" and price_tb.Text <> ""
            dim book_to_remove as item
            book_to_remove.isbn = isbn_tb.Text
            book_to_remove.title = title_tb.Text
            book_to_remove.price = Decimal.Parse(price_tb.Text)
            books.Remove(book_to_remove)
        end if
        
        
        if books.Count = 0 
            first_btn.Enabled = false
            prev_btn.Enabled =  false
            last_btn.Enabled =  false
            next_btn.Enabled =  false
            
            isbn_tb.Text = ""
            title_tb.Text = ""
            price_tb.Text = ""
        else if index = 0
            dim last_book as item = books(index)
            isbn_tb.Text = last_book.isbn
            title_tb.Text = last_book.title
            price_tb.Text = String.Format("{0:00}", last_book.price)
        else
            dim last_book as item = books(index - 1)
            isbn_tb.Text = last_book.isbn
            title_tb.Text = last_book.title
            price_tb.Text = String.Format("{0:00}", last_book.price)
            index -= 1
        end if
   
    end sub
    
    private sub edit_fn(sender as object, args as EventArgs) Handles edit_btn.Click
        edit_enabled = true
        
        isbn_tb.Enabled = false
        title_tb.Enabled = true
        price_tb.Enabled = true
        
        edit_btn.Enabled = false    
        delete_btn.Enabled = false
        new_btn.Enabled = true
        save_btn.Enabled = true    
        
    end sub
End Module